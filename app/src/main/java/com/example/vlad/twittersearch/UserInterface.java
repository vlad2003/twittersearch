package com.example.vlad.twittersearch;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;


public class UserInterface extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_int);
        Intent intent = getIntent();

        TextView a = (TextView) findViewById(R.id.textViewName);
        a.setText("Name is " + intent.getStringExtra("Name"));

        a = (TextView) findViewById(R.id.textViewNickname);
        a.setText("Nickname is " + intent.getStringExtra("Nickname"));

        a = (TextView) findViewById(R.id.textViewUserLocation);
        a.setText("UserLocation is " + (intent.getStringExtra("UserLocation") == "" ? "unknown" : intent.getStringExtra("UserLocation")));

        a = (TextView) findViewById(R.id.textViewFriendsCount);
        a.setText("FriendsCount = " + intent.getStringExtra("FriendsCount"));

        ImageView imageView = (ImageView) findViewById(R.id.imageView);
        String imageUrl = intent.getStringExtra("ImageLink");

        ImageLoader imageLoader = ImageLoader.getInstance(); // Получили экземпляр
        imageLoader.init(ImageLoaderConfiguration.createDefault(this)); // Проинициализировали конфигом по умолчанию
        imageLoader.displayImage(imageUrl, imageView);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_user_int, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
