package com.example.vlad.twittersearch;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.util.ArrayList;
import java.util.List;

import twitter4j.Status;

/**
 * Created by vlad on 7/29/15.
 */
public class ListAdapter extends BaseAdapter
{

    Context ctx;
    LayoutInflater lInflater;

    List<Status> tweets;




    ListAdapter(Context context, List<Status> tweets) {
        ctx = context;
        this.tweets= tweets;
        lInflater = (LayoutInflater) ctx
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        return tweets.size();
    }

    @Override
    public Object getItem(int position) {
        return tweets.get(position);


    }

    @Override
    public long getItemId(int position) {
        return tweets.get(position).getId();


    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;
        if (view == null) {
            view = lInflater.inflate(R.layout.twit, parent, false);
        }

        Status p=tweets.get(position);
        ((TextView) view.findViewById(R.id.twitName)).setText(p.getUser().getScreenName());
        ((TextView) view.findViewById(R.id.twitText)).setText(p.getText());
        ImageView imageView=(ImageView)view.findViewById(R.id.twitImage);
        String imageUrl=p.getUser().getProfileImageURL();

        ImageLoader imageLoader = ImageLoader.getInstance(); // Получили экземпляр
        imageLoader.init(ImageLoaderConfiguration.createDefault(ctx)); // Проинициализировали конфигом по умолчанию
        imageLoader.displayImage(imageUrl, imageView); //
        return view;
    }

}
