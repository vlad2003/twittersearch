package com.example.vlad.twittersearch;

import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import java.util.ArrayList;
import java.util.List;
import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;

public class MainActivity extends ActionBarActivity {
    private Button button;
    private ListAdapter listAdapter;
    private List<Status> tweets;
    private EditText wordsToFind;
    private Twitter twitter;
    private ListView lvMain;
    private ArrayList<String> links;
    private boolean alr = true;
    private Context ctx = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        tweets = new ArrayList<>();
        twitter = new TwitterFactory().getInstance();
        final AccessToken accessToken = new AccessToken(Constants.TOKEN, Constants.TOKEN_SECRET);
        twitter.setOAuthConsumer(Constants.S, Constants.SL);
        twitter.setOAuthAccessToken(accessToken);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        button = (Button) findViewById(R.id.button);
        lvMain = (ListView) findViewById(R.id.listView);
        final Context context = this;
        wordsToFind = (EditText) findViewById(R.id.editText);
        listAdapter = new ListAdapter(context, tweets);
        lvMain.setAdapter(listAdapter);
        lvMain.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {

                    UserFragment frag=UserFragment.newInstance(tweets.get(position).getUser().getName(), tweets.get(position).getUser().getScreenName(),
                            tweets.get(position).getUser().getLocation() == "" ? "unknown" : tweets.get(position).getUser().getLocation(),
                            tweets.get(position).getUser().getFriendsCount() + "",tweets.get(position).getUser().getOriginalProfileImageURL());
                    FragmentTransaction ft = getFragmentManager().beginTransaction();
                    ft.replace(R.id.fragment_user, frag);
                    ft.commit();

                } else {
                    Intent intent = new Intent(ctx, UserInterface.class);
                    intent.putExtra("Name", tweets.get(position).getUser().getName());
                    intent.putExtra("Nickname", tweets.get(position).getUser().getScreenName());
                    intent.putExtra("UserLocation", tweets.get(position).getUser().getLocation());
                    intent.putExtra("ImageLink", tweets.get(position).getUser().getOriginalProfileImageURL());
                    intent.putExtra("FriendsCount", tweets.get(position).getUser().getFriendsCount() + "");

                    startActivity(intent);
                }
            }
        });

        View.OnClickListener oclBtnOk = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alr = false;
                launchRingDialog(lvMain);

                Thread t = new Thread(new Runnable() {
                    public void run() {
                        tweets.clear();
                        try {
                            Query query = new Query(wordsToFind.getText().toString());
                            QueryResult result;
                            result = twitter.search(query);
                            tweets.addAll(result.getTweets());
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Log.d("123", tweets.size() + "");

                                    listAdapter.notifyDataSetChanged();
                                    alr = true;
                                }
                            });
                        } catch (TwitterException te) {
                            te.printStackTrace();
                        }
                    }
                });
                t.start();
            }
        };
        button.setOnClickListener(oclBtnOk);

        lvMain.setOnScrollListener(new AbsListView.OnScrollListener() {
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (lvMain.getLastVisiblePosition() == lvMain.getCount() - 1 && scrollState == 0) {
                    MyTask mt = new MyTask();
                    mt.execute();
                } else if (lvMain.getFirstVisiblePosition() == 0 && scrollState == 0) {
                    MyTask1 mt = new MyTask1();
                    mt.execute();
                }
            }

            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {
            }
        });

        registerForContextMenu(lvMain);
    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        AdapterView.AdapterContextMenuInfo mi = (AdapterView.AdapterContextMenuInfo) menuInfo;
        links = new ArrayList<>();
        String text = tweets.get(mi.position).getText();
        while (text.contains("http")) {
            String link;

            if (text.indexOf(" ", text.indexOf("http")) != -1)
                link = text.substring(text.indexOf("http"), text.indexOf(" ", text.indexOf("http")));
            else
                link = text.substring(text.indexOf("http"));
            links.add(link);
            text = text.replaceFirst("http", "0");
        }

        for (int i = 0; i < links.size(); i++) {
            menu.add(0, i, 0, links.get(i));
        }
    }

    public boolean onContextItemSelected(MenuItem item) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(links.get(item.getItemId())));
        startActivity(browserIntent);

        return super.onContextItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    class MyTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            alr = false;
            launchRingDialog(lvMain);
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                Query query = new Query(wordsToFind.getText().toString());
                QueryResult result;
                query.sinceId(tweets.get(tweets.size() - 1).getId());
                result = twitter.search(query);

                tweets.addAll(result.getTweets());
            } catch (TwitterException te) {
                te.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            listAdapter.notifyDataSetChanged();
            alr = true;
        }
    }

    class MyTask1 extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            alr = false;
            launchRingDialog(lvMain);
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                Query query = new Query(wordsToFind.getText().toString());
                QueryResult result;
                query.setMaxId(tweets.get(0).getId() - 1);
                result = twitter.search(query);

                if (result.getTweets().size() > 0)
                    tweets.addAll(0, result.getTweets());
            } catch (TwitterException te) {
                te.printStackTrace();
            }
            return null;

        }

        @Override
        protected void onPostExecute(Void result) {
            listAdapter.notifyDataSetChanged();
            alr = true;

        }
    }

    public void launchRingDialog(View view) {
        final ProgressDialog ringProgressDialog = ProgressDialog.show(MainActivity.this, "Please wait ...", "Downloading Tweets ...", true);
        ringProgressDialog.setCancelable(true);
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    while (!alr)
                        Thread.sleep(10);
                } catch (Exception e) {

                }
                ringProgressDialog.dismiss();
            }
        }).start();
    }

}


