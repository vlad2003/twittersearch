package com.example.vlad.twittersearch;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import twitter4j.User;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link UserFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link UserFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class UserFragment extends Fragment {
    private String Name;
    private String Nickname;
    private String UserLocation;
    private String FriendsCount;
    private String URL;

    public static UserFragment newInstance(String Name, String Nickname, String UserLocation, String FriendsCount, String URL) {
        UserFragment fragment = new UserFragment();
        Bundle args = new Bundle();
        args.putString("Name", Name);
        args.putString("Nickname", Nickname);
        args.putString("UserLocation", UserLocation);
        args.putString("FriendsCount", FriendsCount);
        args.putString("URL", URL);
        fragment.setArguments(args);
        return fragment;
    }

    public UserFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (getArguments() != null) {
            Name = getArguments().getString("Name");
            Nickname = getArguments().getString("Nickname");
            UserLocation = getArguments().getString("UserLocation");
            FriendsCount = getArguments().getString("FriendsCount");
            URL = getArguments().getString("URL");
        }

        View v = inflater.inflate(R.layout.fragment_user, null);
        TextView name = (TextView) v.findViewById(R.id.textViewName);
        TextView nickname = (TextView) v.findViewById(R.id.textViewNickname);
        TextView userLocation = (TextView) v.findViewById(R.id.textViewUserLocation);
        TextView friendsCount = (TextView) v.findViewById(R.id.textViewFriendsCount);
        ImageView imageView = (ImageView) v.findViewById(R.id.imageView);

        name.setText(Name);
        nickname.setText(Nickname);
        userLocation.setText(UserLocation);
        friendsCount.setText(FriendsCount);

        ImageLoader imageLoader = ImageLoader.getInstance(); // Получили экземпляр
        imageLoader.init(ImageLoaderConfiguration.createDefault(getActivity())); // Проинициализировали конфигом по умолчанию
        imageLoader.displayImage(URL, imageView);

        // Inflate the layout for this fragment
        return v;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
